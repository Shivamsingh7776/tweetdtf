from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Tweet
from .serializer import TweetSerializer
# Create your views here.


@api_view(['GET', 'POST', 'PATCH', 'DELETE'])
def get_home(request):

    if request.method == 'GET':
        return Response({
            'status': '200',
            'message': 'Django restframework is working',
            'method': 'GET'
        })
    elif request.method == 'POST':
        return Response({
            'status': '200',
            'message': 'Django restframework is working',
            'method': 'POST'
        })
    elif request.method == 'PUT':
        return Response({
            'status': '200',
            'message': 'Django restframework is working',
            'method': 'PUT'
        })
    elif request.method == 'DELETE':
        return Response({
            'status': '200',
            'message': 'Django restframework is working',
            'method': 'DELETE'
        })
    elif request.method == 'PATCH':
        return Response({
            'status': '200',
            'message': 'Django restframework is working',
            'method': 'PATCH'
        })


@api_view(['POST'])
def post_tweet(request):
    try:
        data = request.data  # data will come
        serializer = TweetSerializer(data=data)

        if serializer.is_valid():
            serializer.save()
            print(serializer.data)

            return Response({
                'status': True,
                'message': 'success data',
                'data': serializer.data
            })

        return Response({
            'status': False,
            'message': 'Invalid data',
            'data': serializer.errors
        })

    except Exception as e:
        print(e)

    return Response({
        'status': False,
        'message': 'Invalid value given',
        'data': {}
    })


@api_view(['GET'])
def get_tweet(request):
    todo_obj = Tweet.objects.all()
    serializer = TweetSerializer(todo_obj, many=True)

    return Response({
        'status': True,
        'message': 'Todo fetched',
        'data': serializer.data
    })


@api_view(['PATCH'])
def patch_tweet(request):
    try:
        data = request.data

        if not data.get('user'):
            return Response({
                'status': False,
                'message': 'user name is required',
                'data': {}
            })

        obj = Tweet.objects.get(user=data.get('user'))
        serializer = TweetSerializer(obj, data=data, partial=True)

        if serializer.is_valid():
            serializer.save()
            return Response({
                'status': True,
                'message': 'Success data',
                'data': serializer.data
            })
        return Response({
            'status': False,
            'message': 'user name is required',
            'data': serializer.errors
        })

    except Exception as e:
        print(e)

    return Response({
        'status': False,
        'message': 'invalid user',
        'data': {}
    })
