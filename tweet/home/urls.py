from django.urls import path
from .views import get_home, post_tweet, get_tweet, patch_tweet

urlpatterns = [
    path('', get_home, name='get-user'),
    path('get-tweet/',  get_tweet, name='get-todo'),
    path('post-tweet/', post_tweet, name='post-todo'),
    path('patch-tweet/', patch_tweet, name='patch-tweet')
]