from django.db import models
import uuid
# Create your models here.


class BaseModel(models.Model):
    uid = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4())
    created_at = models.DateField(auto_created=True)
    updated_at = models.DateField(auto_now_add=True)


    class Meta:
        abstract = True

class Tweet(models.Model):
    user = models.CharField(max_length=100)
    tweet_title = models.CharField(max_length=100)
    tweet_description = models.CharField(max_length=100)
    is_done = models.BooleanField(default=False)
