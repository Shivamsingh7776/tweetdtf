from rest_framework import serializers
import re
from .models import Tweet

from django.template.defaultfilters import slugify

class TweetSerializer(serializers.ModelSerializer):
    # slug = serializers.SerializerMethodField()
    class Meta:
        model = Tweet

        fields = '__all__'
    

    # def get_slug(self, obj):
    #     return slugify(obj.todo_title)

    # def validate(self, validated_data):
    #     print(validated_data)
        
    #     if validated_data.get('todo_title'):
    #         todo_title = validated_data['todo_title']
    #         regex = re.compile('^[A-Za-z_][A-Za-z0-9_]*')
            
    #         if len(todo_title) < 3:
    #             raise serializers.ValidationError('Todo title must be grater that 3 charecter!')


    #         if not regex.search(todo_title) == None:
    #             raise serializers.ValidationError("Todo title can not contains special character")
    #         return validated_data
